<?php

namespace controllers;

use core\View;
use models\UserModel;

class SesController extends \core\Controller {

    function __construct() {
        parent::__construct();
        $this->model = new UserModel();
        $this->view = new View();
    }

    function actionIndex() {
        $this->actionLogin();
    }

    function actionLogin() {
        if( (integer)$this->user >0 ) $this->reload('/task/list');

        if ( count($_POST)>0 ) {
            $obj = $_POST;
            if($_POST['action'] == 'login' && (integer)$this->user == 0 ) {
                if( $this->model->login($obj) )
                    $this->reload('/task/list');
            }
        }

        $fields = $this->parseFields(['login','password'],$obj);

        $this->view->generate('forms/login', 'main'
            ,[
                'isSave' => true,
                'columns' => $this->model->getShowColumns(),
                'exitUrl' => '/task/list',
                'fields' => $fields,
            ]
        );
    }
}