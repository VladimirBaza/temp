<?php

namespace controllers;

class MainController extends \core\Controller {
    function actionIndex() {
        (new TaskController())->actionIndex();
    }
}