<?php

namespace controllers;

use core\View;
use models\TaskModel;
use models\UserModel;

class TaskController extends \core\Controller {

    function __construct() {
        parent::__construct();
        if (!isset($_SESSION['taskNumberPage'])) $_SESSION['taskNumberPage'] = 1;

        $this->model = new TaskModel();
        $this->view = new View();
    }

    function actionIndex() {
        $this->actionList();
    }

    function actionList() {
        $this->model->setOrderBy($_SESSION['taskSort']);
        $this->model->setNumberPage($_SESSION['taskNumberPage']);
        $data = $this->model->getData($this->model->getShowColumns());

        $users = new UserModel();
        foreach ($data as $i => $r) {
            $data[$i]['edit_admin'] = $this->model->getHtmlColumn('edit_admin', $data[$i]['edit_admin']);
            $data[$i]['status_id'] = $this->model->getHtmlColumn('status_id', $data[$i]['status_id']);
        }

        $this->view->generate('tasks', 'main'
            ,[
                'user' => (integer)$this->user>0 ? $users->getDataById((integer)$this->user) : null,
                'blnEdit' => (integer)$this->user > 0,
                'columns' => $this->model->getShowColumns(),
                'columnSort' => array_keys($_SESSION['taskSort']),
                'rows' => $data,
                'numberPase' => $this->model->getNumberPage(),
                'coutTotalPages' => $this->model->getCoutTotalPages(),
            ]
        );
    }

    function actionView() {
        $obj = $this->model->getDataById( (integer)$_GET['id'] );
        if ( count($_POST)>0 ) {
            $obj = $_POST;
            $obj['id'] = (integer)$_GET['id'];
            if($_POST['action'] == 'save' && ((integer)$this->user > 0 || (integer)$obj['id'] == 0)
            ) {
                if( $this->model->save($obj) ) $this->reload('/task/list');
            }
        }

        if( (integer)$this->user == 0 && (integer)$obj['id'] > 0 )
            $this->reload('/task/list');

        $fields = $this->parseFields(['text','user','email',((integer)$this->user > 0 ? 'status_id' : '')],$obj);

        $this->view->generate('forms/task', 'main'
            ,[
                'user' => (integer)$this->user>0 ? (new UserModel())->getDataById((integer)$this->user) : null,
                'blnEdit' => (integer)$this->user > 0,
                'isDelete' => true,
                'isSave' => false,
                'exitUrl' => '/task/list',
                'columns' => $this->model->getShowColumns(),
                'fields' => $fields,
            ]
        );
    }
}