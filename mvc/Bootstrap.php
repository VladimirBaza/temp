<?php
session_start();
function myAutoLoader($className) { require_once str_replace('\\','/',$className) . '.php'; }
spl_autoload_register('myAutoLoader');
core\Route::start();