<?php

namespace core;

use PDO;
use PDOException;

class Model {

    protected $DB;
    protected $orderBy = [];
    protected $numberPage = 1;
    protected $countPages = 3;

    function __construct() {

        $dsn = 'mysql:dbname='.DB_DBNAME.';host=127.0.0.1';
        try {
            $this->DB = new PDO($dsn, DB_USER, DB_PASSWD);
        } catch (PDOException $e) {
            echo 'Подключение не удалось: ' . $e->getMessage();
        }
    }

    public static function getTableName() {

    }

    public static function getColumns() {

    }

    public function getShowColumns($listShowColumns = []) {
        if ( $listShowColumns == [] )
            $listShowColumns = array_keys( $this->getColumns() );
        return array_filter(
            $this->getColumns()
            , function($k)  use ($listShowColumns) {
                return mb_substr_count(','.implode(',',$listShowColumns).',',','.$k.',')>0 ? true : false;
                    //array_search( $k, $listShowColumns );
            }
            , ARRAY_FILTER_USE_KEY
        );
    }

    public static function getHtmlColumn($column,$val) {

    }


    public function setNumberPage($n) {
        $this->numberPage = $n;
    }
    public function getNumberPage() {
        return $this->numberPage;
    }

    public function setOrderBy($a) {
        $this->orderBy = $a;
    }

    public function getData($listColumns = []) {
        $sth = $this->DB->prepare(
            sprintf("select %s from %s %s %s"
                ,implode(',', array_keys($listColumns == [] ? $this->getColumns() : $listColumns) )
                ,$this->getTableName()
                , count($this->orderBy)>0
                    ? ' order by '. implode(',',array_map(function($k, $v) {return "$k $v";}, array_keys($this->orderBy), $this->orderBy) )
                    : ''
                , $this->numberPage>0 ? ' limit '.(($this->numberPage-1)*$this->countPages).','.$this->countPages  : ''
            )
        );
        $sth->execute();
        return $sth->fetchAll( PDO::FETCH_ASSOC );
    }

    public function getDataById($id,$listColumns = []) {
        $sth = $this->DB->prepare(
            sprintf("select %s from %s where %s"
                ,implode(',', array_keys($listColumns == [] ? $this->getColumns() : $listColumns) )
                ,$this->getTableName()
                ,"id=".$id)
        );

        $sth->execute();
        return $sth->fetch( PDO::FETCH_ASSOC );
    }

    public function getCoutTotalPages() {
        $cntRows = $this->getTotalRows();
        return ceil($cntRows / $this->countPages);
    }

    public function getTotalRows() {
        $sth = $this->DB->prepare( sprintf("select count(*) as cnt from %s" ,$this->getTableName()) );
        $sth->execute();
        return (integer)$sth->fetch( PDO::FETCH_ASSOC )['cnt'];
    }

    public function getFieldForColumn($column) {
        return '';
    }

    public function getValidate($data) {
        return false;
    }

    public function save($data) {

    }


    public function setFieldsError($f,$e) {
        $_SESSION['errors'][$this->getTableName()][$f]=$e;
    }

    public function getFieldsError($f) {
        $e = $_SESSION['errors'][$this->getTableName()][$f];
        unset($_SESSION['errors'][$this->getTableName()][$f]);
        return $e;
    }

}