<?php

namespace core;

use \models\SesModel;

class Route {
    static function start() {
        static::setGetParams();

        $controller_name = 'Main';
        $action_name = 'index';

        $uri = count($_GET) > 0
        ? mb_substr($_SERVER['REQUEST_URI'],0,mb_strpos($_SERVER['REQUEST_URI'],'?'))
        : $_SERVER['REQUEST_URI'];
        $routes = explode('/', $uri);

        if (!empty($routes[1])) $controller_name = $routes[1];
        if (!empty($routes[2])) $action_name = $routes[2];

        $controller_name = 'controllers\\'.ucfirst($controller_name) . 'Controller';
        $action_name = 'action' . ucfirst($action_name);

        $controller = new $controller_name;

        if (method_exists($controller, $action_name)) {
            $controller->$action_name();
        } else
            Route::ErrorPage404();

    }

    function ErrorPage404() {
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:' . $host . '404');
    }

    private function setGetParams() {
        if(count($_GET) > 0) {
            if(isset($_GET['type']) && isset($_GET['setp'])) {
                header("Location: ".mb_substr($_SERVER['REQUEST_URI'],0,mb_strpos($_SERVER['REQUEST_URI'],'?')) );
                $_SESSION[$_GET['type'].'NumberPage'] = 0;
                if($_GET['type']=='' || $_GET['setp']=='') exit;
                $_SESSION[$_GET['type'].'NumberPage'] = $_GET['setp'];
                exit;
            }

            if(isset($_GET['type']) && isset($_GET['setsort']) && isset($_GET['settypesort'])) {
                header("Location: ".mb_substr($_SERVER['REQUEST_URI'],0,mb_strpos($_SERVER['REQUEST_URI'],'?')) );
                $_SESSION[$_GET['type'].'Sort']=[];
                if($_GET['type']=='' || $_GET['setsort']=='' || $_GET['settypesort']=='') exit;
                $_SESSION[$_GET['type'].'Sort'][ $_GET['setsort'] ] = $_GET['settypesort'] == 1 ? DB_SORT_ASC : ($_GET['settypesort'] == 2 ? DB_SORT_DESC : DB_SORT_ASC);
                exit;
            }

            if( isset($_GET['logout']) ) {
                header("Location: ".mb_substr($_SERVER['REQUEST_URI'],0,mb_strpos($_SERVER['REQUEST_URI'],'?')) );
                (new SesModel())->setSesUser(0);
                exit;
            }
        }
    }
}