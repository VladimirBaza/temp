<?php

namespace core;

class View {
    
    function generate($contentView, $templateView, $data = null) {
        $data['alerts'] = $_SESSION['alerts'];
        if(is_array($data)) extract($data);
        include 'mvc/views/'.$templateView.'.php';
    }
}