<?php
namespace core;

use \models\SesModel;

class Controller {

    protected $user = 0;
    public $model;
    public $view;

    function __construct() {
        $this->view = new View();
        $ses = new SesModel();
        $ses->sesControl();
        $this->user = $ses->getSesUser();
    }

    function actionIndex() {

    }

    public function parseFields($fields = [],$data = []) {
        if ($fields == []) return [];
        $fs = [];
        foreach ($fields as $field) {
            $fs[$field] = $this->model->getFieldForColumn($field, $data[$field]);
            if ($e = $this->model->getFieldsError($field))
                $fs[$field].= sprintf('<div class="alert alert-danger py-1 px-2 mt-1">%s</div>',$e);
        }

        return $fs;
    }

    public function reload($uri = null) {
        header("Location: ".($uri == null ? $_SERVER['REQUEST_URI'] : $uri) );
        exit;
    }

}