<?php
/**
 * @var array $user
 */
?>

<div class="mt-4"></div>
<div class="alert alert-secondary w-75 m-auto row">
    <?= $user ? '' : $user['login'] ?>
    <?php if($user): ?>
        Логин: <?= $user['login'] ?>
        <a class="ml-3" href="#" onclick="javascript: location.href='?logout=1';void(0);">Выход</a>
    <?php else: ?>
        Гость <a class="ml-3" href="#" onclick="javascript: location.href='/ses/login';void(0);">Войти</a>
    <?php endif; ?>
</div>
<div class="mt-4">
</div>
