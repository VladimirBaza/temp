<?php
/**
 * @var array $columns
 * @var array $fields
 * @var boolean $isDelete
 * @var boolean $isSave
 * @var string $exitUrl
 */
?>
<div class="alert alert-secondary w-50 m-auto row">
    <form method="post" class="container-fluid">
        <?php foreach ($fields as $a => $c): ?>
            <div class="row pt-2">
                <div class="col-4 text-right"><?= $columns[$a] ?></div>
                <div class="col-8"><?= $c ?></div>
            </div>
        <?php endforeach; ?>
        <div class="row">
            <div class="col-12 pt-4 text-right">
                <input type="hidden" name="action" id="action" value="">
                <?php if($isDelete): ?>
                    <button type="submit" onclick="javascript: $('#action').val('save'); return true;" class="btn btn-primary">Сохранить</button>
                <?php endif; ?>
                <?php if($isSave): ?>
                    <button type="submit" onclick="javascript: $('#action').val('delete'); return true;" class="btn btn-danger">Удалить</button>
                <?php endif; ?>
                <a class="btn btn-secondary" href="#" onclick="javascript: location.href='<?= $exitUrl ?>';void(0);">Назад</a>
            </div>
        </div>
    </form>
</div>


