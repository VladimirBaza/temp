<?php
/**
 * @var array $columns
 * @var array $rows
 * @var array $columnSort
 * @var integer $numberPase
 * @var integer $coutTotalPages
 * @var boolean $blnEdit
 */
?>
<div class="alert alert-secondary w-75 m-auto row">
    <div class="col-2">
        Сортировать:
    <select class="form-control" onchange="javascript: location.href='?type=task&setsort='+this.value+'&settypesort=1';void(0);">
        <option value="">Без сортировки</option>
        <?php foreach ($columns as $a => $c): ?>
            <option <?= $columnSort[0] == $a ? 'selected' : '' ?> value="<?= $a ?>"><?= $c ?></option>
        <?php endforeach; ?>
    </select>
    </div>
    <div class="col-2"></div>
    <div class="col-4 text-center">
        <?php if ($coutTotalPages > 1): ?>
            <nav class="d-inline-block" aria-label="...">
                <ul class="pagination">
                    <?php for($i=1; $i <= $coutTotalPages; $i++): ?>
                        <li class="page-item <?= $numberPase == $i ? 'active' : '' ?>">
                            <a class="page-link" href="#" onclick="javascript: location.href='?type=task&setp=<?= $i ?>';void(0);"><?= $i ?></a>
                        </li>
                    <?php endfor; ?>
                </ul>
            </nav>
        <?php endif; ?>
    </div>

    <div class="col-4 text-right">
        <a class="btn btn-primary" href="#" onclick="javascript: location.href='/task/view?id=0';void(0);">Новая задача</a>
    </div>
</div>

<table class="table w-75 m-auto">
    <thead>
        <tr>
            <?= $blnEdit ? '<th></th>' : '' ?>
            <?php foreach ($columns as $cn => $c) if($cn!='id'): ?>
                <th><?= $c ?></th>
            <?php endif; ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($rows as $r): ?>
            <tr>
                <?= $blnEdit ? '<td><a class="btn btn-primary" href="#" onclick="javascript: location.href=\'/task/view?id='.$r['id'].'\';void(0);">Изменить</a></td>' : '' ?>
                <?php foreach ($r as $cn => $c) if($cn!='id'): ?>
                    <td><?= $c ?></td>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>