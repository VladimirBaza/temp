<?php
/**
 * @var array $alerts
 */
?>
<?php foreach ($alerts as $alert): ?>
    <div class="mt-4"></div>
    <div class="alert alert-info w-75 m-auto row user-alert">
            <?= $alert ?>
    </div>
<?php endforeach; ?>

<script>
    $(document).ready(function (){
        setTimeout(
            function () {
                $('.user-alert').each(function() {
                    $(this).hide(200);
                });
            }

            ,1000
        );
    });
</script>
<?php
$_SESSION['alerts'] = [];
?>


