<?php

namespace models;

class SesModel extends \core\Model {

    public static function getTableName() {
        return 'ses';
    }

    public static function getColumns() {
        return [
            'ses_id' => 'ID',
            'user_id' => 'user',
            'dt' => 'date',
        ];
    }

    public function getShowColumns() {
        return [];
    }

    public function sesControl() {
        $sth = $this->DB->prepare(
            sprintf("select ses_id from %s where ses_id='%s'",$this->getTableName(),session_id())
        );
        $sth->execute();
        if( $sth->rowCount()>0 ) {
            $sth = $this->DB->prepare(
                sprintf("update %s SET dt=now() where ses_id='%s'",$this->getTableName(),session_id())
            );
            $sth->execute();
        }
        else {
            $sth = $this->DB->prepare(
                sprintf("insert into %s(ses_id,dt,user_id) values ('%s',now(),0)",$this->getTableName(),session_id())
            );
            $sth->execute();
        }
        $sth = $this->DB->prepare(
            sprintf("select %s from ses where dt + INTERVAL 30 MINUTE < now()",$this->getTableName())
        );
        $sth->execute();
    }

    public function setSesUser($user) {
        $sth = $this->DB->prepare(
            sprintf("update %s SET dt=now(),user_id=%s where ses_id='%s'"
                ,$this->getTableName()
                ,$user
                ,session_id())
        );
        $sth->execute();

    }

    public function getSesUser() {
        $sth = $this->DB->prepare(
            sprintf("select user_id from %s where ses_id='%s'",$this->getTableName(),session_id())
        );
        $sth->execute();
        return $sth->fetch()['user_id'];
    }

}