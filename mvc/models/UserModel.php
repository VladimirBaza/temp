<?php

namespace models;

use PDO;

class UserModel extends \core\Model {
    protected $userId;

    public static function getTableName() {
        return 'users';
    }

    public static function getColumns() {
        return [
            'id' => 'ID',
            'login' => 'Login',
            'password' => 'Пароль',
        ];
    }

    public function getShowColumns() {
        $listShowColumns =
            [
                'id','login','password'
            ];
        return parent::getShowColumns($listShowColumns);
    }

    public function getFieldForColumn($column,$v = "") {
        switch ($column) {
            case 'login':
                return sprintf('<input class="form-control" name="%s" type="text" value="%s">',$column,$v);
            case 'password':
                return sprintf('<input class="form-control" name="%s" type="password" value="%s">',$column,$v);
            default:
                return '';
        }
    }

    public function getValidate($data) {
        $this->userId = 0;

        $bln = true;
        if ($data['login'] == '') {
            $this->setFieldsError('login','Введите login');
            $bln = false;
        }
        if ($data['password'] == '') {
            $this->setFieldsError('password','Введите пароль');
            $bln = false;
        }

        if ( $data['password'] == '') {
            $this->setFieldsError('password','Введите пароль');
            $bln = false;
        }

        if($bln == true ) {
            $o = $this->DB->prepare(
                sprintf("select id from %s where login='%s' and password='%s'", $this->getTableName(), $data['login'],$data['password'])
            );
            $o->execute();
            if($o->rowCount() != 1) {
                $this->setFieldsError('password','Не верное имя пользователя или пароль');
                $bln = false;
            } else
                $this->userId = $o->fetch( PDO::FETCH_ASSOC )['id'];
        }

        return $bln;
    }

    public function login($data) {
        if ( $this->getValidate($data) ) {
            (new SesModel())->setSesUser($this->userId);
            return true;
        }
        return false;
    }
}