<?php

namespace models;

use PDO;

class TaskModel extends \core\Model {

    public static function getTableName() {
        return 'tasks';
    }

    public static function getColumns() {
        return [
            'id' => 'ID',
            'text' => 'Текст задачи',
            'user' => 'Пользователь',
            'email' => 'Email',
            'status_id' => 'Статус',
            'edit_admin' => 'Отредактировано администратором',
        ];
    }

    public function getShowColumns() {
        $listShowColumns =
            [
                'id','text','user','email','status_id','edit_admin'
            ];
        return parent::getShowColumns($listShowColumns);
    }

    public static function getHtmlColumn($column,$val) {
        switch ($column) {
            case 'status_id':
                return $val==1 ? 'Выполнена' : 'Не выполнена';
            case 'edit_admin':
                return $val==1 ? 'Да' : '';
            default:
                return '';
        }
    }

    public function getFieldForColumn($column,$v = "") {
        switch ($column) {
            case 'text':
                return sprintf('<input class="form-control" name="%s" type="text" value="%s">',$column,$v);
            case 'user':
                return sprintf('<input class="form-control" name="%s" type="text" value="%s">',$column,$v);
            case 'email':
                return sprintf('<input class="form-control" name="%s" type="text" value="%s">',$column,$v);
            case 'status_id':
                return sprintf('<select class="form-control" name="%s" value="%s">
                                <option %s value="0">Не выполнена</option>
                                <option %s value="1">Выполнена</option>
                                </select>',$column,$v,($v == 0 ? 'selected' : ''),($v == 1 ? 'selected' : '') );
            default:
                return '';
        }
    }

    public function getValidate($data) {
        $bln = true;
        if ($data['text'] == '') {
            $this->setFieldsError('text','Заполните поле');
            $bln = false;
        }
        if ($data['user'] == '') {
            $this->setFieldsError('user','Заполните поле');
            $bln = false;
        }
        if ($data['email'] == '') {
            $this->setFieldsError('email','Заполните поле');
            $bln = false;
        }
        if (!preg_match("/^(?:[a-z0-9]+(?:[-_.]?[a-z0-9]+)?@[a-z0-9_.-]+(?:\.?[a-z0-9]+)?\.[a-z]{2,5})$/i", $data['email'])) {
            $this->setFieldsError('email','Email указан не корректно');
            $bln = false;
        }
        if ($data['status_id'] != 0 && $data['status_id'] != 1) {
            $this->setFieldsError('status_id','Значение не верно');
            $bln = false;
        }

        return $bln;
    }

    public function save($data) {
        if ( $this->getValidate($data) ) {

            if($data['id'] == 0) {
                $sth = $this->DB->prepare(
                    sprintf("insert into %s(text,user,email) values ('%s','%s','%s')"
                        , $this->getTableName()
                        , htmlspecialchars($data['text'])
                        , htmlspecialchars($data['user'])
                        , htmlspecialchars($data['email'])
                    )
                );
                $_SESSION['alerts'][] = 'Добавлена новая задача "'.htmlspecialchars($data['text']).'"';
            }
            else {
                $o = $this->DB->prepare(
                    sprintf("select text,user,email from %s where id = %s", $this->getTableName(), $data['id'])
                );
                $o->execute();
                $o = $o->fetch( PDO::FETCH_ASSOC );

                $edit_admin = 0;
                if ($o['text'] != $data['text'] || $o['user'] != $data['user'] || $o['email'] != $data['email'])
                    $edit_admin = 1;

                $sth = $this->DB->prepare(
                    sprintf("update %s set text='%s',user='%s',email='%s',edit_admin=%s,status_id=%s where id = %s"
                        , $this->getTableName()
                        , htmlspecialchars($data['text'])
                        , htmlspecialchars($data['user'])
                        , htmlspecialchars($data['email'])
                        ,$edit_admin
                        ,htmlspecialchars($data['status_id'])
                        ,$data['id']
                    )
                );

            }
            $sth->execute();
            return true;
        }
        return false;
    }
}