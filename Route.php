<?php

class Route {
    static function start() {
        static::setGetParams();

        // контроллер и действие по умолчанию
        $controller_name = 'Main';
        $action_name = 'index';

        $routes = explode('/', $_SERVER['REQUEST_URI']);

        if (!empty($routes[1])) {
            $controller_name = $routes[1];
        }

        // получаем имя экшена
        if (!empty($routes[2])) {
            $action_name = $routes[2];
        }

        // добавляем префиксы
        $model_name = ucfirst($controller_name) . 'Model';
        $controller_name = ucfirst($controller_name) . 'Controller';
        $action_name = 'action' . ucfirst($action_name);

        // подцепляем файл с классом модели (файла модели может и не быть)
        $model_file = "mvc/models/" . $model_name . '.php';
        if (file_exists($model_file)) include $model_file;

        // подцепляем файл с классом контроллера
        $controller_file = "mvc/controllers/" . $controller_name . '.php';
        if (file_exists($controller_file)) {
            include $controller_file;
        } else
            Route::ErrorPage404();

        // создаем контроллер
        $controller = new $controller_name;
        $action = $action_name;

        if (method_exists($controller, $action)) {
            // вызываем действие контроллера
            $controller->$action();
        } else
            Route::ErrorPage404();

    }

    function ErrorPage404() {
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:' . $host . '404');
    }

    private function setGetParams() {
        if(count($_GET) > 0) {
            if(isset($_GET['setp'])) {
                $_SESSION['taskNumberPage'] = $_GET['setp'];
                header("Location: ".mb_substr($_SERVER['REQUEST_URI'],0,mb_strpos($_SERVER['REQUEST_URI'],'?')) );
                exit;
            }
        }
    }
}